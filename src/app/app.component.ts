import { Component } from '@angular/core';
import { GameTickService } from './services/game-tick.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'needy-cats';

  constructor(tickService: GameTickService) {
    tickService.watchTick().subscribe({
      next: () => console.log(new Date(Date.now()).toTimeString(), "Tick")
    })
  }
}
