import { Injectable } from '@angular/core';
import { interval } from 'rxjs';

const tickObserver = interval(500);

@Injectable()
export class GameTickService {
  watchTick() {
    return tickObserver;
  }
}